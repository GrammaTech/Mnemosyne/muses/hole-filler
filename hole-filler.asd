(defsystem "hole-filler"
    :name "hole-filler"
    :author "GrammaTech"
    :licence "MIT"
    :description "Fill holes in Argot program sketches"
    :long-description "Yada"
    :depends-on (:hole-filler/hole-filler)
    :class :package-inferred-system
    :build-operation "asdf:program-op"
    :build-pathname "hole-filler"
    :entry-point "hole-filler/server::run-server"
    :perform
    (test-op (o c) (symbol-call :hole-filler '#:test))
    :defsystem-depends-on (:asdf-package-system))   ; ?
