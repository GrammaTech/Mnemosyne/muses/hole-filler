(defpackage :hole-filler/hole-filler
  (:nicknames :hole-filler)
  (:use :gt/full
        :software-evolution-library
        :software-evolution-library/software/parseable
        :software-evolution-library/software/lisp
        :software-evolution-library/software/javascript
        :stefil :command-line-arguments)
  (:export :filler))
(in-package :hole-filler/hole-filler)
(in-readtable :curry-compose-reader-macros)

;;; The idea throughout is that we're trying to turn a SPECification of a
;;; function definition that may contain holes (represented by a DEFUN form
;;; with question marks indicating holes) into a FUNction definition with any
;;; holes filled by the function's parameters. In theory this will lead to more
;;; interesting hole-filling strategies later; for now it's just an elementary
;;; prototype.

;; Convert SPEC from a specification of a function definition to an actual
;; function and return that function.
;; This is probably the sloppiest way to do this, as it affects the global
;; environment, which is highly undesirable. It should use
;; ALEXANDRIA:NAMED-LAMBDA instead.
;; And it assumes that SPEC is in the right format.
(defun spec->fun (spec)
  (eval spec))

;; Test whether FN's behavior conforms to EXAMPLES.
;; EXAMPLES should be of the form
;; (((x11 x12 … x1k) . y1) ((x21 x22 … x2k) . y2) … ((xn1 xn2 … xnk) . yn))
;; and FN's behavior conforms to example i if (equalp (fn xi1 xi2 … xik) yi).
;; For example, one might use the following definition of EXAMPLES to test an
;; addition function.
;; (defvar examples '(((2 2) . 4) ((10 10) . 20)))
;; Of course this is very naive and will easily break or get stuck in a loop.
;; Making long conformity checks time out and also not eat the whole stack would
;; be nice.
(defun conformsp (fn examples)
  (if (null examples)
      t
      (and (fset:equal? (apply fn (caar examples)) (cdar examples))
           (conformsp fn (cdr examples)))))

;; Generate all permutations, with replacements, of K members of ELEMENTS.
;; This returns a list that grows in size exponentially with K, so it's not a
;; very good idea.
(defun perms-rep (elements k)
  (if (zerop k)
      '(())
      (apply #'append
             (mapcar (lambda (x) (mapcar {cons _ x} elements))
                     (perms-rep elements (1- k))))))

;; Count the holes in SPEC.
(defun holes-count (spec)
  (cond
    ((null spec) 0)
    ((equalp (car spec) '?)
     (1+ (holes-count (cdr spec))))
    ((consp (car spec)) (+ (holes-count (car spec)) (holes-count (cdr spec))))
    (t (holes-count (cdr spec)))))

;; Get the lambda list of SPEC.
;; Stuff will break if SPEC isn't actually a specification of a function.
(defun args (spec) (when (eq (car spec) 'defun) (caddr spec)))

;; Fill the holes in SPEC according to ASSIGNMENTS.
;; Which is to say, fill the ith hole with the ith element of ASSIGNMENTS.
(defun filling (spec assignments)
  (cond
    ((null spec) (values () assignments))
    ((atom spec) (values spec assignments))
    ((eq (car spec) '?)
     (mvlet ((spec-1 assignments-1 (filling (cdr spec) (cdr assignments))))
       (values (cons (car assignments) spec-1) assignments-1)))
    (t
     (mvlet* ((spec-1 assignments-1 (filling (car spec) assignments))
              (spec-2 assignments-2 (filling (cdr spec) assignments-1)))
       (values (cons spec-1 spec-2) assignments-2)))))

;; Try to fill the holes in the body of the function represented by SPEC with
;; the function's parameters so that its behavior conforms to EXAMPLES.
;; Return a list of all such filled copies of SPEC.
;; Maybe this should return a list of assignments instead? That would be more
;; useful for an IDE to receive.
(defun filler (spec examples)
  (remove-if-not (lambda (spec)
                   (ignore-errors (conformsp (spec->fun spec) examples)))
                 (mapcar {filling spec}
                         (perms-rep (args spec) (holes-count spec)))))


;;;; Main test suite.
(defsuite test)
(in-suite test)

;; Most or all tests for FILLER should look roughly the same…
;; SPEC should be a function specification with holes (quoted, so that when
;; evaluated it produces the specification and doesn't try to define the
;; function); EXAMPLES should be a (quoted) list of examples in the format
;; specified above; CONDITION should be a condition that the RESult must
;; satisfy.
(defmacro filler-test-skeleton ((res) &body (spec examples condition))
  (once-only (spec examples)
    `(macrolet ((in-res (spec)
                  (list 'member spec ',res :test '#'tree-equal)))
       (let* ((,res (filler ,spec ,examples)))
         (is ,condition)))))

;; Do we get only the trival result for a specification with no holes?
(deftest no-holes ()
  (filler-test-skeleton
      (res)
      '(defun complete (x y) (* x y))
      ()
      (fset:equal? res '((defun complete (x y) (* x y))))))

;; Do we get both possibilities for filling two interchangeable holes?
(deftest addition-commutativity ()
  (filler-test-skeleton
      (res)
    '(defun add (x y) (+ ? ?))
    '(((0 0) . 0) ((1 0) . 1) ((0 1) . 1))
    (and
     (= (length res) 2)
     (in-res '(defun add (x y) (+ x y)))
     (in-res '(defun add (x y) (+ y x))))))

;; Do we get the only viable possibility for filling two noninterchangeable
;; holes?
(deftest subtraction-noncommutativity ()
  (filler-test-skeleton
      (res)
   '(defun sub (x y) (- ? ?))
   '(((0 0) . 0) ((1 0) . 1) ((0 1) . -1))
   
   (fset:equal? res '((defun sub (x y) (- x y))))))

;; Can we handle recursion?
;; Spoiler: not with this version of the program.
;; (deftest recursive ()
;;   (filler-test-skeleton
;;    '(defun app (x y)
;;      (if (null ?)
;;          ?
;;          (cons (car ?) (app (cdr ?) ?))))
;;    '(((() ()) . ()) (((a) (b c)) . (a b c)))
;;    res
;;    (in-res '(defun app (x y)
;;              (if (null x)
;;                  y
;;                  (cons (car x) (app (cdr x) y)))))))


;;;; External command-line driver.
;;;
;;; Compile with the following command:
;;;   sbcl --eval '(ql:quickload :hole-filler)' \
;;;        --eval '(asdf:make :hole-filler :type :program :monolithic t)'
;;;
;; (define-command server ()
;;   "" "" nil)
