Hole Filler
===========

A synthesis module to fill holes in Argot sketches

> NOTE: This project is very new and not yet fit for use.

# Abstract

TODO

## Copyright and Acknowledgments

Copyright (C) 2020 GrammaTech, Inc.

This code is licensed under the MIT license. See the LICENSE file in
the project root for license terms.

The project or effort depicted was sponsored by the Air Force Research
Laboratory (AFRL) and the Defense Advanced Research Projects Agency
(DARPA) under contract no. FA8750-15-C-0113. Any opinions, findings,
and conclusions or recommendations expressed in this material are
those of the author(s) and do not necessarily reflect the views of
AFRL or DARPA.
